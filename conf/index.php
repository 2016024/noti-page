<?php
session_start();
if(isset($_SESSION["id"])){
	if($_SESSION["id"] == 'admin'){
		header('Location: server/main.php');
		exit();		
	}else{
		echo "권한 없는 사용자로 로그인 되었습니다. 다시 로그인해주세요.";
		session_destroy();
	}
}else{
	#header('Location: ../login.html');
	#exit();
	include 'login.html';
}
?>